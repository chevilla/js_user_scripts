/*

  koha_bibagos : script utilisateur pour personnaliser l'interface de Koha par
  les gestionnaires de la BibAGOS à l'Inria Sophia Antipolis.

  Copyright (C) 2023 de

  Centre de recherche Inria Sophia Antipolis Méditerranée,
  Équipe Factas,
  Sophia Antipolis, France.

  Auteur : S. Chevillard
  sylvain.chevillard@ens-lyon.org

  Ce programme est un logiciel libre: vous pouvez le redistribuer
  et/ou le modifier selon les termes de la "GNU General Public
  License", tels que publiés par la "Free Software Foundation"; soit
  la version 3 de cette licence ou (à votre choix) toute version
  ultérieure.

  Ce programme est distribué dans l'espoir qu'il sera utile, mais
  SANS AUCUNE GARANTIE, ni explicite ni implicite; sans même les
  garanties de commercialisation ou d'adaptation dans un but spécifique.

  Se référer à la "GNU General Public License" pour plus de détails.
  Elle est disponible ici : http://www.gnu.org/licenses/gpl-3.0.html

  Vous devriez avoir reçu une copie de la "GNU General Public License"
  en même temps que ce programme; sinon, écrivez a la "Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA".

*/

// ==UserScript==
// @name         koha_bibagos
// @namespace    http://gitlab.inria.fr/
// @version      7.0
// @description  Personnaliser l'interface de Koha pour la BibAGOS
// @author       Sylvain Chevillard
// @match      https://emprunts-agos-admin.inria.fr/*
// ==/UserScript==

(function() {
    'use strict';

    function masqueExemplairesExternes() {
	var L = document.querySelectorAll('.modal-header')

	var test = 0;
	var i = 0;
	var logmsg="";

	while(!test) {
	    test = (L[i].innerText.search('Code à barres non trouvé')) >= 0;
	    i = i+1;
	    if (i >= L.length) break;
	}

	if (test) {
	    var page_modale = L[i-1];
	    var tab = page_modale.parentElement.querySelector(".table_borrowers");
	    var rows = tab.querySelectorAll('tr');
	    for(const row of rows) {
		var content = row.querySelectorAll('td');
		if (content.length >= 2) {
		    var code_barre = content[1].innerText.trim();
		    logmsg += "Lecture de |"+code_barre+"|";
		    if (code_barre.substr(0,3) != "SOP") {
			row.style = "display:none;";
			logmsg += " (rendu invisible)";
		    }
		    logmsg += "\n";
		}
	    }
	    console.log(logmsg);
	}
    }

    function ajouteNouvelExemplaire() {
	var nouveaute, localisation, code_barre, typepret, msg;
	nouveaute = document.querySelector("#subfield995B").querySelector("select");
	nouveaute.value = "1";
	nouveaute.querySelector("option[value='1']").selected=1;
	nouveaute.dispatchEvent(new Event('change'))
	localisation = document.querySelector("#subfield995e").querySelector("select");
	localisation.value = "SOP_MED";
	localisation.querySelector("option[value='SOP_MED']").selected=1;
	localisation.dispatchEvent(new Event('change'))
	code_barre = document.querySelector("#subfield995f").querySelector("input");
	set_barcode(code_barre.id, false);
	typepret = document.querySelector("#subfield995r").querySelector("select");
	typepret.value = "PS";
	typepret.querySelector("option[value='PS']").selected=1;
	typepret.dispatchEvent(new Event('change'));
	msg = "Valider l'auto-complétion suivante ?\n\n  Nouveauté : oui\n  Localisation : Médiathèque Sophia\n  code barre : "+code_barre.value+"\n  Type de prêt : prêt sans caution réservable.";
	if(confirm(msg)) {
	    document.querySelector("input[name='add_submit']").click();
	}
    }

    function listePermissionsDisponibles() {
	var listeCategories, e, listeSousCategories, k, t, s;
	s = "";
	listeCategories = document.querySelector("div.permissions").querySelectorAll("div.parent");
	for (e of listeCategories) {
	    s += e.querySelector("input").id+" ("+e.id+")\n";
            listeSousCategories = e.querySelectorAll("div.child-flags");
            for(k=0; k<listeSousCategories.length; k++) {
		t = listeSousCategories[k];
		s += "  "+t.querySelector("input").id+"\n";
	    }
	}
	console.log(s);
    }

    function afficheProfilsPermissions() {
	var profils, selectionDiv, contenu, p, code, c, d;
	listePermissionsDisponibles();
	profils = {};
	profils.gestionnaire = ["flag-1", "flag-2", "flag-6", "flag-12", "flag-14", "borrowers_delete_borrowers", "borrowers_edit_borrowers", "editcatalogue_delete_all_items", "editcatalogue_edit_catalogue", "editcatalogue_edit_items"];
	selectionDiv = document.createElement("div");
	selectionDiv.id = "profilsType";
	selectionDiv.style = "padding-bottom: 20px;";
	contenu = "<p>Positionner les permissions correspondant à un profil-type :</p>\n";
	for(p in profils) {
	    code = "event.preventDefault(); document.getElementById('UncheckAllFlags').click(); ";
	    for(c of profils[p]) {
		code += "document.getElementById('"+c+"').checked=true; ";
	    }
	    contenu += "<button onclick=\""+code+"\">"+p+"</button>";
	}
	selectionDiv.innerHTML = contenu;
	d = document.querySelector("#permissionstree");
	d.parentNode.insertBefore(selectionDiv, d);
    }

    function personnaliseAjoutNotice(n) {
	var categorieActuelle = document.querySelector("#toolbar a.change-framework").attributes["data-frameworkcode"].nodeValue;
	console.log("n="+n+" : "+categorieActuelle);
	if (n > 0) setTimeout( () => {personnaliseAjoutNotice(n-1);}, 100);
	
	/*
	  if (categorieActuelle != "BOOK") {
	    document.querySelector("#settings-menu a[data-frameworkcode='BOOK']").click();
	}
	else {
	    var categorie = document.querySelector("select[data-category='TYPEDOC']");
	    for (const e of categorie.querySelectorAll("option")) {
		if ((e.value != "BD") && (e.value != "LIVRE")) {
		    e.remove();
		}
	    }
	    categorie.value = "BD";
	    categorie.querySelector("option[value='BD']").selected=1;
	    categorie.dispatchEvent(new Event('change'))
	    }
	    */
    }

    /* Déclenchement des actions au chargement de la page */
    if (/circulation.pl/.test(window.location.href)) {
	masqueExemplairesExternes();
    }
    if (/additem.pl/.test(window.location.href)) {
	ajouteNouvelExemplaire();
    }
    if (/member-flags.pl/.test(window.location.href)) {
	afficheProfilsPermissions();
    }
    if (/addbiblio.pl/.test(window.location.href)) {
	 personnaliseAjoutNotice(100); 
    }
})();
